#!/bin/sh -xeu
grep --color=auto -nF -r --exclude='./.git/*' --exclude='./*/__pycache__/*' \
	--exclude='./build/*' --exclude='*.c' --exclude='*.so' "$1" .
