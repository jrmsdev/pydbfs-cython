# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

from dbfs.cli cimport parser
from dbfs.core import logger

log = logger.get_child (__name__)

cpdef int run ():
    args = parser.parse_args ()
    if args is None:
        return 1
    logger.configure (args)
    return args.cmd (args)

cpdef int system_mount ():
    return 128
