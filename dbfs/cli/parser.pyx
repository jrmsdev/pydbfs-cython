# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

from argparse import ArgumentParser
from dbfs.core.cmd cimport mount

cdef object parse_args (argv = None):
    parser = ArgumentParser (prog = 'dbfs')
    parser.add_argument ('-d', '--debug', action = 'store_true', default = False)

    subparser = parser.add_subparsers (description = None)
    mount.add_menu (subparser)

    return check_args (parser, argv)

cdef object check_args (parser, argv):
    args = parser.parse_args (argv)
    if not hasattr (args, 'cmd'):
        args.cmd = None
        parser.print_help ()
        return None
    return args
