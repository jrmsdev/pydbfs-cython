# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import sys
from os import path, system
from distutils.extension import Extension

BUILD_EXT = (
    'dbfs.cli.main',
    'dbfs.cli.parser',
    'dbfs.core.cmd.mount',
    'dbfs.core.cmd.newfs',
    'dbfs.core.fusew',
    'dbfs.core.libfuse',
    'dbfs.core.storage',
    'dbfs.core.storage_server',
    'dbfs.core.task',
)

def extensions ():
    r = list ()
    for e in BUILD_EXT:
        fn = e.replace ('.', path.sep)
        fn += '.pyx'
        r.append (Extension (e, [fn]))
    return r

def compileall ():
    if path.isfile ('./setup.py'):
        print ('build compileall ...')
        rc = system ('./setup.py -q build_ext -i')
        if rc == 0:
            rc = system ('./setup.py -q build')
        if rc != 0:
            print ('build failed!')
            sys.exit (2)
