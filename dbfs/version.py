# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

VMAJOR = 0
VMINOR = 0
VPATCH = 0
VERSION = None

def __setversion ():
    global VERSION
    VERSION = f'{VMAJOR}.{VMINOR}'
    if VPATCH > 0:
        VERSION = f'{VERSION}.{VPATCH}'

__setversion ()
