# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import multiprocessing as mp
from dbfs.core import logger

log = logger.get_child (__name__)

cdef class Server (object):
    cdef object __ctx
    cdef object __lock
    cdef object __parent_q
    cdef object __child_q
    cdef str __name

    def __init__ (self, name):
        log.debug (f'server __init__ {name}')
        self.__ctx = mp.get_context ()
        self.__lock = self.__ctx.Lock ()
        self.__parent_q = self.__ctx.Queue ()
        self.__child_q = self.__ctx.Queue ()
        self.__name = name

    def __del__ (self):
        log.debug (f'server __del__ {self.__name}')
        self.__child_q.close ()
        self.__parent_q.close ()

    def send (self, cmd, *args):
        log.debug (f'server {self.__name} send {cmd}  {args}')

    def recv (self):
        log.debug (f'server {self.__name} recv')

cdef object server (str name, object mainfunc):
    log.debug (f'{name} server')
    log.debug (f'{name} mainfunc {id (mainfunc)}')
    return Server (name)
