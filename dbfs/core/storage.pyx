# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

from dbfs.core import logger
from dbfs.core cimport storage_server

log = logger.get_child (__name__)

cdef object context (str uri):
    log.debug (f'create context {uri}')
    s = storage_server.start (uri)
    return StorageContext (s, uri)

cdef class StorageContext (object):
    cdef str __uri
    cdef bint __fail
    cdef bint __is_open
    cdef object __server

    def __init__ (self, object server, str uri):
        self.__uri = uri
        self.__fail = 0
        self.__is_open = 0
        self.__server = server

    # storage open
    def __enter__ (self):
        log.debug (f'ctx __enter__ {self.__uri}')
        self.open ()
        if not self.failed ():
            self.__is_open = 1
        return self

    # storage close
    def __exit__ (self, exc_type, exc, exc_tb):
        log.debug (f'ctx Exception: {exc_type} {exc} {exc_tb}')
        if self.__is_open:
            self.close ()
            if not self.failed ():
                self.__is_open = 0
        else:
            log.debug ('ctx __exit__: storage is not open')
        storage_server.stop (self, self.__server)
        log.debug (f'ctx __exit__ {self.__uri}')

    def open (self):
        log.debug ('open: %s', self.__uri)
        self.__is_open = 1

    def close (self):
        log.debug ('close: %s', self.__uri)
        self.__is_open = 0

    def failed (self):
        return self.__fail
