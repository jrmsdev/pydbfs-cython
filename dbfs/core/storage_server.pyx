# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

from dbfs.core import logger

from dbfs.core cimport task

log = logger.get_child (__name__)

cdef object start (str uri):
    log.debug (f'start {uri} {id (request)}')
    return task.server ('storage', request)

cdef stop (object ctx, object srvr):
    log.debug (f'stop {id (srvr)}')

cdef request (object ctx, object srvr):
    log.debug (f'request {id (ctx)} {id (srvr)}')
