# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import os
import sys
from dbfs.core import logger

log = logger.get_child (__name__)
__libfuse = None

cdef loadlib (str use_fuse = 'fusepy'):
    if running_as_root ():
        print ('ERROR: do not run as root')
        sys.exit (1)
    if use_fuse == 'fusepy':
        load_fusepy ()
    else:
        print ('ERROR: fuse lib not specified')
        sys.exit (1)
    log.debug (f'libfuse {use_fuse} loaded')

cdef int main (object ctx, str mountpoint, object storage, bint debug):
    log.debug (f'main {mountpoint}')
    dbfs_ops = None # pass storage
    try:
        return __libfuse.main (mountpoint, dbfs_ops, debug)
    except Exception as err:
        log.debug (err)
        raise

cdef bint running_as_root ():
    if 0 in (os.getuid (), os.geteuid ()):
        return 1
    return 0

cdef load_fusepy ():
    global __libfuse
    from dbfs.core.libw import fusepyw
    __libfuse = fusepyw
