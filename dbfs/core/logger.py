# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import logging

# docs.p.o/3.6/howto/logging.html#changing-the-format-of-displayed-messages
LOG_FORMAT = '[%(process)s %(thread)s] %(name)s: %(message)s'

def configure (args):
    if args.debug:
        logging.basicConfig (level = logging.DEBUG, format = LOG_FORMAT)
#~         os.environ['PYTHONASYNCIODEBUG'] = '1'

__log = logging.getLogger ('dbfs')

def get_child (name):
    return __log.getChild (name[5:])
