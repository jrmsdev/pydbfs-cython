# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

from dbfs.core import logger
from dbfs.core cimport libfuse

log = logger.get_child (__name__)

cdef main (object ctx):
    log.debug ('main')
    libfuse.loadlib ()
    libfuse.main (ctx, "fake_mountpoint", None, True)
