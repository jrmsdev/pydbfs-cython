# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import sys
from dbfs.core import logger

log = logger.get_child (__name__)

try:
    import fuse
except ModuleNotFoundError:
    print ('ERROR: fuse module not found - try: pip install fusepy')
    sys.exit (1)
except OSError as err:
    print ('ERROR:', err)
    sys.exit (1)

from fuse import FUSE

def main (mountpoint, dbfs_ops, debug):
    log.debug (f'main {mountpoint} {dbfs_ops}')
    try:
        FUSE (dbfs_ops, mountpoint, foreground = debug)
    except RuntimeError as err:
        return err.args[0]
    return 0
