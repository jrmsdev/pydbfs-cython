# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

from dbfs.core import logger
from dbfs.core cimport fusew, storage

log = logger.get_child (__name__)

cdef add_menu (object subparser):
    p = subparser.add_parser ('mount', help = 'mount a filesystem')
    p.add_argument ('storage', help = 'file system storage')
    p.set_defaults (cmd = mount_cmd)

cdef int mount_cmd (object args):
    log.debug ('mount_cmd')
    try:
        with storage.context (args.storage) as ctx:
            fusew.main (ctx)
    except:
        raise
    if ctx.failed ():
        print (ctx.error ())
        return ctx.retcode ()
    return 128
