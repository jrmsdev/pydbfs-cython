<a name="top"></a>

# dbfs

* [cli][src.cli.main]
    * [parse args][src.cli.parser]
        * build help menu from all commands
        * parse args
        * check args or fail
    * [configure loggin][src.core.logger]
    * [dispatch command](#doc.core.cmd)

<a name="doc.core.cmd"></a> [^](#top)

* cmd
    * [mount][src.core.cmd.mount]
        * create [storage context](#doc.core.storage)
        * [fusew main](#doc.core.fusew) under current storage context
        * report errors
    * newfs
        * storage newfs
        * report errors

<a name="doc.core.storage"></a> [^](#top)

* [storage][src.core.storage]
    * context
        * [start storage server](#doc.core.storage_server)
        * create an storage context using started server
        * when context enter
            * storage_open
        * when context exit
            * storage_close
            * [stop storage server](#doc.core.storage_server)
    * storage_open
        * lock context
        * storage engine connect
        * do basic fs check
    * storage_close
        * do basic fs check
        * storage engine disconnect
        * unlock context
    * newfs

<a name="doc.core.storage_server"></a> [^](#top)

* [storage_server][src.core.storage_server]
    * start
        * launch a [server task](#doc.core.task) for request manager
    * stop
        * stop the server
    * request
        * wait for an storage request
        * process the request
        * check errors
        * return response

<a name="doc.core.fusew"></a> [^](#top)

* [fusew][src.core.fusew]
    * main
        * [load libfuse](#doc.core.libfuse)
        * load operations for opened storage
            * create pool of workers
            * loop until umount or failure
                * wait for operation request
                * create an operation request context
                * lock context
                * if lock failed
                    * stop all workers
                    * manage errors
                    * abort loop
                * put a worker from the pool to procees the request
                * wait for the context to finish
                * if context failed
                    * stop all workers
                    * manage errors or exceptions
                    * unlock context
                    * abort loop
                * else
                    * unlock context and start the loop again
        * start [libfuse main](#doc.core.libfuse) process

<a name="doc.core.libfuse"></a> [^](#top)

* [libfuse][src.core.libfuse]
    * loadlib
        * fail if running as root
        * load requested or default fuse lib to use
    * main
        * wrap the call to underlying fuse main loop

<a name="doc.core.task"></a> [^](#top)

* [task][src.core.task]
    * server
        * launch child process using a queue for input/output operations

[src.cli.main]:                  ../dbfs/cli/main.pxd
[src.cli.parser]:                ../dbfs/cli/parser.pxd
[src.core.cmd.mount]:            ../dbfs/core/cmd/mount.pxd
[src.core.fusew]:                ../dbfs/core/fusew.pxd
[src.core.libfuse]:              ../dbfs/core/libfuse.pxd
[src.core.logger]:               ../dbfs/core/logger.pxd
[src.core.storage]:              ../dbfs/core/storage.pxd
[src.core.storage_server]:       ../dbfs/core/storage_server.pxd
[src.core.task]:                 ../dbfs/core/task.pxd
