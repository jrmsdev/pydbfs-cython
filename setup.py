#!/usr/bin/env python3

# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

from setuptools import setup
from Cython.Compiler import Options
from Cython.Build import build_ext
from dbfs import build

Options.docstrings = False
Options.fast_fail = True

setup (
    ext_modules = build.extensions (),
    cmdclass = {'build_ext': build_ext},
)
